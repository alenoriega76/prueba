const express = require('express');
const router = express.Router();
//const getRandomInt = require('../../ejemplo_express/aleatorio')

let pedidos  = require('../models/pedidos');

/**
 * @swagger
 * /comentarios/:
 *  get:
 *    description: lista todos los pedidos
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
  res.json(pedidos);
});

/**
 * @swagger
 * /comentarios:
 *  post:
 *    description: crea un pedidos
 *    parameters:
 *    - name: id
 *      description: Id del pedido
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: topico_id
 *      description: id del topico del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: usuario_id
 *      description: id del usuario del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: comentario
 *      description: comentario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/pedidos/alta', (req, res) => {
  const nuevoPedido = req.body;
  nuevoPedido.id = getRandomInt(1, 10000);
  pedidos.push(nuevoPedido);
  res.json({msj: `pedido agregado correctamente`});
});


/**
 * @swagger
 * /comentarios/{id}:
 *  delete:
 *    description: elimina un pedido de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del pedido
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id/borar', (req, res) => {
  const id_pedido= parseInt(req.params.id);
  pedidos = pedidos.filter(pedido => pedido.id != id_pedido);
  res.json({msj: `pedido eliminado correctamente`});
});

/**
 * @swagger
 * /comentarios:
 *  put:
 *    description: actualiza un pedido
 *    parameters:
 *    - name: id
 *      description: Id del pedido
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: topico_id
 *      description: id del topico del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: usuario_id
 *      description: id del usuario del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: comentario
 *      description: descripcion
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/pedidos/modificado', (req, res) => {
  id_pedido = req.body.id;
  const indicepedido = pedidos.findIndex(x => x.id == id_peido);

  const object = {
    id: parseInt(req.body.id),
    producto_id: parseInt(req.body.topico_id),
    usuario_id: parseInt(req.body.usuario_id),
    comentario: req.body.comentario
  };
  pedidos[indicepedido] = object;

  res.json({msj: `pedido actualizado correctamente`});
});

module.exports = router;
