const express = require('express');
const router = express.Router();
//const getRandomInt = require('./rutas/productos')

let productos  = require('../models/productos');

/**
 * @swagger
 * /productos/:
 *  get:
 *    description: lista todos los productos
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
  res.json(productos);
});

/**
 * @swagger
 * /topicos:
 *  post:
 *    description: crea un topico
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: titulo
 *      description: nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: descripcion del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/productos/alta', (req, res) => {
  const nuevoProducto = req.body;
  nuevoProducto.id = getRandomInt(1, 10000);
  topicos.push(nuevoProducto);
  res.json({msj: `producto agregado correctamente`});
});


/**
 * @swagger
 * /topicos/{id}:
 *  delete:
 *    description: elimina un productode acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id/borrar', (req, res) => {
  const id_producto= parseInt(req.params.id);
  productos = productos.filter(producto => producto.id != id_producto);
  res.json({msj: `producto eliminado correctamente`});
});

/**
 * @swagger
 * /topicos:
 *  put:
 *    description: actualiza un producto
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: titulo
 *      description: nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: descripcion del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/productos/modificado', (req, res) => {
  id_producto = req.body.id;
  const indiceproducto = productos.findIndex(x => x.id == id_producto);

  const object = {
    id: parseInt(req.body.id),
    titulo: req.body.titulo,
    descripcion: req.body.descripcion
  };
  productos[indiceproducto] = object;

  res.json({msj: `producto actualizado correctamente`});
});

module.exports = router;
