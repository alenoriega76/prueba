const express= require('express');
const app = express();
const port = 3005
const rutaUsuarios=require('./rutas/usuarios');
const rutaPedidos=require('./rutas/pedidos');
const rutaProductos=require('./rutas/productos');
const swaggerJsDoc = require('swagger-jsdoc');

const swaggerUI = require('swagger-ui-express');
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Delilha Resto',
        version: '1.0.0'
      }
    },
    apis: ['./app.js'],
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use('/api-docs',swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));
   app.use('/usuarios',rutaUsuarios);
   app.use('/pedidos',rutaPedidos);
   app.use('/productos',rutaProductos);
  

app.listen();{
  console.log(`escuchando en http://localhost:${port}`)
}


